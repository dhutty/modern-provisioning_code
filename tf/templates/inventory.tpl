${connection_string_node}

[dh_tf_class:vars]
ansible_ssh_common_args='-o ControlMaster=auto -o ControlPersist=60s -o StrictHostKeyChecking=no'
ansible_ssh_pipelining=True
${ssh_user_string}
${ssh_private_key_path_string}

[dh_tf_class]
${list_node}
